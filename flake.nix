{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=release-24.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      with nixpkgs.legacyPackages.${system};
      let
        t = lib.trivial;
        hl = haskell.lib;
        hsPkgs = haskell.packages.ghc983;

        project = hsPkgs.developPackage {
          root = lib.sourceFilesBySuffices ./. [ ".cabal" ".hs" "LICENSE" ];
          name = "ghinge";

          modifier = (t.flip t.pipe) [
            hl.dontHaddock
            hl.enableStaticLibraries
            hl.justStaticExecutables
            hl.disableLibraryProfiling
            hl.disableExecutableProfiling
          ];
        };

      in {
        packages.default = project;

        apps.default =
          flake-utils.lib.mkApp { drv = self.packages.${system}.default; };

        devShells.default = hsPkgs.shellFor {
          packages = _: [ self.packages.${system}.default ];
          nativeBuildInputs = with pkgs;
            with hsPkgs; [
              cabal-gild
              cabal-install
              haskell-language-server
            ];
        };
      });

  nixConfig = {
    extra-substituters = [ "https://magthe-dev.cachix.org" ];
    extra-trusted-public-keys = [
      "magthe-dev.cachix.org-1:mFSHoML7X60LmeIf+vvmuG6uBHdufg8kcUkcfSV3yHc="
    ];
  };
}
