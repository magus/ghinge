module Main where

import Data.Aeson ((.=))
import Data.Aeson qualified as A
import Data.ByteString.Char8 qualified as BSC
import Data.ByteString.Lazy.Char8 qualified as BSLC
import Data.Foldable (toList)
import Data.Function ((&))
import Data.Text qualified as T
import GHC.Generics (Generic)
import GitHub qualified as GH
import System.Envy (FromEnv, decodeEnv)
import System.Exit (exitFailure)
import System.IO (hPrint, stderr)

newtype GHConfig = GHConfig
    { githubToken :: BSC.ByteString
    }
    deriving (Eq, Show, Generic)

instance FromEnv GHConfig

main :: IO ()
main = do
    cfg <-
        decodeEnv @GHConfig >>= \case
            Left e -> do
                BSLC.putStrLn $ A.encode $ errObj "config error"
                hPrint stderr e
                exitFailure
            Right r -> pure r
    req (githubToken cfg) >>= \case
        Left e -> do
            BSLC.putStrLn $ A.encode $ errObj "GH error"
            hPrint stderr e
            exitFailure
        Right res -> do
            let tooltip = T.intercalate "\n" $ map notificationToTooltip (toList res)
            BSLC.putStrLn $ A.encode $ outObj (length res) tooltip
  where
    req token = GH.github (GH.OAuth token) GH.getNotificationsR GH.FetchAll
    errObj :: String -> A.Value
    errObj txt =
        A.object
            [ "text" .= txt
            , "alt" .= txt
            , "tooltip" .= txt
            , "class" .= calcClass 666
            , "percentage" .= (100 :: Int)
            ]
    outObj cnt tooltip =
        A.object
            [ "text" .= show cnt
            , "alt" .= ("no alt" :: String)
            , "tooltip" .= tooltip
            , "class" .= calcClass cnt
            , "percentage" .= calcPercentage cnt
            ]
    calcClass :: Int -> String
    calcClass cnt
        | cnt == 0 = "none"
        | cnt < 3 = "few"
        | otherwise = "many"
    calcPercentage :: Int -> Int
    calcPercentage cnt
        | cnt <= 0 = 0
        | cnt < 3 = 50
        | otherwise = 100

notificationToTooltip :: GH.Notification -> T.Text
notificationToTooltip noti = T.intercalate " - " [subj, repo, reason]
  where
    subj = noti & GH.notificationSubject & GH.subjectTitle
    repo = noti & GH.notificationRepo & GH.repoRefRepo & GH.untagName
    reason = case noti & GH.notificationReason of
        GH.AssignReason -> "assign"
        GH.AuthorReason -> "author"
        GH.CommentReason -> "comment"
        GH.InvitationReason -> "invitation"
        GH.ManualReason -> "manual"
        GH.MentionReason -> "mention"
        GH.ReviewRequestedReason -> "review"
        GH.StateChangeReason -> "state change"
        GH.SubscribedReason -> "subscribed"
        GH.TeamMentionReason -> "team mention"
